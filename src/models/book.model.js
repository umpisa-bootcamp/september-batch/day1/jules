const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const bookSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
bookSchema.plugin(toJSON);
bookSchema.plugin(paginate);

/**
 * Check if title is taken
 * @param {string} title - The book's title
 * @param {ObjectId} [excludeBookId] - The id of the book to be excluded
 * @returns {Promise<boolean>}
 */
bookSchema.statics.isTitleTaken = async function (title, excludeBookId) {
  const book = await this.findOne({ title, _id: { $ne: excludeBookId } });
  return !!book;
};

/**
 * @typedef Book
 */
const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
