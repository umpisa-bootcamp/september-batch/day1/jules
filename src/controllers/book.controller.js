const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { bookService } = require('../services');

const listBooks = catchAsync(async (req, res) => {
  const books = await bookService.listBooks()
  return res.json({
    data: books
  })
})

const createBook = catchAsync(async (req, res) => {
  const book = await bookService.createBook(req.body)
  return res.json({
    data: book
  })
})

const updateBook = catchAsync(async (req, res) => {
  const book = await bookService.updateBookById(req.params.id, req.body)
  return res.json({
    data: book
  })
})

const deleteBook = catchAsync(async (req, res) => {
  const book = await bookService.deleteBookById(req.params.id)
  return res.status(204).end()
})

const getBook = catchAsync(async (req, res) => {
  const book = await bookService.getBookById(req.params.id)
  return res.json({
    data: book
  })
})

module.exports = {
  listBooks,
  createBook,
  updateBook,
  deleteBook,
  getBook,
};
