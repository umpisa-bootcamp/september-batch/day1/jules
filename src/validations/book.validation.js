const Joi = require('@hapi/joi');
const { password } = require('./custom.validation');

const listBooks = {};

const createBook = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
  }),
};

const updateBook = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
  }),
};

const deleteBook = {};

const getBook = {};

module.exports = {
  listBooks,
  createBook,
  getBook,
  deleteBook,
  updateBook,
};
